﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using WpfApp1.Models;
using MessageBox = System.Windows.MessageBox;

namespace WpfApp1
{
    public class FileExplorer : ViewModelBase
    {
        private DirectoryInfoViewModel _root;
        private object _selectedItem;
        private string _statusMessage;

        public RelayCommand CommandOpenRootDir { get; private set; }
        public RelayCommand CommandSort { get; private set; }
        
        public RelayCommand OpenTextFile { get; private set; }
        
        public event EventHandler<string> OpenTextFileEvent;

        public SortModel SortModel { get; private set; }

        public FileExplorer()
        {
            CommandOpenRootDir = new RelayCommand(HandleOpenRootDir);
            CommandSort = new RelayCommand(HandleSort, CanExecuteHandleSort);
            OpenTextFile = new RelayCommand(HandleOpenTextFile, CanExecuteHandleOpenTextFile);
            SortModel = new SortModel();
            SortModel.PropertyChanged += (o, e) =>
            {
                Globals.threadIds = new HashSet<int>();
                Globals.tokenSource = new CancellationTokenSource();
                Globals.token = Globals.tokenSource.Token;
                BtnVis = Visibility.Visible;
                Task.Factory.StartNew(async () =>
                {
                    await _root.Sort(o as SortModel);
                    StatusMessage = "Koniec sortowania, wątków: " + Globals.threadIds.Count;
                    Debug.WriteLine("Total threads: "+Globals.threadIds.Count);
                    BtnVis = Visibility.Collapsed;
                }, Globals.token);
            };
            OnPropertyChanged(nameof(Lang));
        }

        private bool CanExecuteHandleOpenTextFile(object selectedItem)
        {
            return selectedItem is FileInfoViewModel fileModel &&
                   ((IList) new[] {".txt", ".xml", ".sh", ".c", ".cs", ".csproj", ".php",}).Contains(fileModel.Model.Extension.ToLower());
        }

        private void HandleOpenTextFile(object selectedItem)
        {
            using (var textReader = File.OpenText((selectedItem as FileInfoViewModel)?.Model.FullName))
            {
                OpenTextFileEvent?.Invoke(this, textReader.ReadToEnd());
            }
        }

        private bool CanExecuteHandleSort(object obj)
        {
            return _root != null;
        }

        private void HandleSort(object obj)
        {
            var window = new SortDialog(SortModel);
            window.Show();
        }

        private void HandleOpenRootDir(object parameters)
        {
            var d = new FolderBrowserDialog
            {
                Description = Strings.SelectDirectoryToOpen
            };
            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Task.Factory.StartNew(() =>
                {
                    OpenRoot(d.SelectedPath);
                }, TaskCreationOptions.PreferFairness);
            }
        }

        public DirectoryInfoViewModel Root
        {
            get => _root;
            set
            {
                if (_root == value) return;
                _root = value;
                OnPropertyChanged();
            }
        }
        
        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                if (value != null)
                {
                    _statusMessage = value;
                    OnPropertyChanged("StatusMessage");
                }
            }
        }

        public string Lang
        {
            get { return CultureInfo.CurrentUICulture.TwoLetterISOLanguageName; }
            set
            {
                if (value != null)
                    if (CultureInfo.CurrentUICulture.TwoLetterISOLanguageName != value)
                    {
                        CultureInfo.CurrentUICulture = new CultureInfo(value);
                        OnPropertyChanged();
                    }
            }
        }

        private Visibility _btnVis;
        public Visibility BtnVis
        {
            get { return _btnVis; }
            set
            {
                if (value != null)
                {
                    _btnVis = value;
                    OnPropertyChanged();
                }
            }
        }


        public async Task OpenRoot(string path)
        {
            BtnVis = Visibility.Collapsed;
            Root = new DirectoryInfoViewModel(this);
            Root.PropertyChanged += Root_PropertyChanged;
            await Root.Open(path);
            StatusMessage = "Ready";
        }

        private void Root_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "StatusMessage" && sender is FileSystemInfoViewModel viewModel)
                StatusMessage = viewModel.StatusMessage;
        }

        public void selectedItem(object treeViewSelectedItem)
        {
            _selectedItem = treeViewSelectedItem;
        }
    }
}