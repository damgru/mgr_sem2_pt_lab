﻿using System.Collections.Generic;
using System.Threading;

namespace WpfApp1
{
    static class Globals
    {
        public static HashSet<int> threadIds = new HashSet<int>();
        public static CancellationTokenSource tokenSource = new CancellationTokenSource();
        public static CancellationToken token = tokenSource.Token;
    }
}