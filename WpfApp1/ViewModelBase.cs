﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using WpfApp1.Annotations;
using WpfApp1.Models;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace WpfApp1
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public Exception Exception;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class FileSystemInfoViewModel : ViewModelBase
    {
        private FileSystemInfo model;
        public string Path;
        public string _imagePath;
        public object Owner { get; protected set; }

        public FileSystemInfoViewModel(object owner)
        {
            this.Owner = owner;
        }
        
        public string StatusMessage
        {
            get { return statusMessage; }
            set
            {
                if (value != null)
                {
                    statusMessage = value;
                    OnPropertyChanged("StatusMessage");
                }
            }
        }

        public string statusMessage { get; set; }

        public FileExplorer OwnerExplorer
        {
            get
            {
                var owner = Owner;
                while (owner is DirectoryInfoViewModel directoryInfoViewModel)
                {
                    if (directoryInfoViewModel.Owner is FileExplorer explorer)
                    {
                        return explorer;
                    }

                    owner = directoryInfoViewModel.Owner;
                }

                return null;
            }
        }

        public FileSystemInfo Model
        {
            get => model;
            set
            {
                if (model == value) return;
                model = value;
                ImagePath = SelectProperIcon(value);
                OnPropertyChanged();
            }
        }

        public string ImagePath
        {
            get => _imagePath;
            set
            {
                if (_imagePath == value) return;
                _imagePath = value;
                OnPropertyChanged();
            }
        }

        public long Size { get; set; }

        private static string SelectProperIcon(FileSystemInfo info)
        {
            if (info is DirectoryInfo)
            {
                return "/images/folder_closed.png";
            }

            return info.Extension.ToLower() switch
            {
                ".pdf" => "/images/pdf.png",
                ".txt" => "/images/text_file.png",
                ".png" => "/images/picture.png",
                ".jpg" => "/images/picture.png",
                _ => "/images/document.png"
            };
        }
    }

    public class FileInfoViewModel : FileSystemInfoViewModel
    {
        private bool _isNodeExpanded = false;
        public ICommand OpenTextFileCmd => OwnerExplorer.OpenTextFile;

        public FileInfoViewModel(FileSystemInfoViewModel owner): base(owner)
        {
            
        }

        public bool IsNodeExpanded
        {
            get => _isNodeExpanded;
            set
            {
                _isNodeExpanded = value;
                OnPropertyChanged();
            }
        }
    }

    public class DirectoryInfoViewModel : FileSystemInfoViewModel
    {
        private DispatchedObservableCollection<FileSystemInfoViewModel> _items = new DispatchedObservableCollection<FileSystemInfoViewModel>();
        public DispatchedObservableCollection<FileSystemInfoViewModel> Items { 
            get => _items;
            private set
            {
                _items = value;
                OnPropertyChanged();
            } 
        }

        public DirectoryInfoViewModel(object owner) : base(owner)
        {
        }

        public FileSystemWatcher Watcher;

        private bool _isNodeExpanded = true;
        public bool IsNodeExpanded
        {
            get => _isNodeExpanded;
            set
            {
                _isNodeExpanded = value;
                OnPropertyChanged();
            }
        }

        public void InitWatchers()
        {
            Watcher = new FileSystemWatcher(Path);
            Watcher.Created += OnFileSystemChanged;
            Watcher.Renamed += OnFileSystemChanged;
            Watcher.Deleted += OnFileSystemChanged;
            Watcher.Changed += OnFileSystemChanged;
            Watcher.EnableRaisingEvents = true;
        }

        public async Task Open(string path)
        {
            Path = path;
            StatusMessage = path;
            await loadTree(path);
            InitWatchers();
            StatusMessage = "Ready";
        }

        private async Task loadTree(string path)
        {
            await Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Clear()));

            foreach (var dirName in Directory.GetDirectories(path))
            {
                var dirInfo = new DirectoryInfo(dirName);
                DirectoryInfoViewModel itemViewModel = new DirectoryInfoViewModel(this);
                Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemViewModel));
                itemViewModel.Model = dirInfo;
                itemViewModel.Size = dirInfo.EnumerateFileSystemInfos().Count();
                itemViewModel.StatusMessage = path;
                await Task.Delay(1);
                
                Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Add(itemViewModel)));
                await Task.Factory.StartNew(() => itemViewModel.Open(dirInfo.FullName));
                //Items.Add(itemViewModel);
                await Task.Delay(1);
            }

            foreach (var fileName in Directory.GetFiles(path))
            {
                var fileInfo = new FileInfo(fileName);
                FileInfoViewModel itemViewModel = new FileInfoViewModel( this );
                Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemViewModel));
                itemViewModel.StatusMessage = path;
                await Task.Delay(1);
                itemViewModel.Model = fileInfo;
                itemViewModel.Size = fileInfo.Length;
                Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Add(itemViewModel)));
                //Items.Add(itemViewModel);
                await Task.Delay(1);
            }
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems.Cast<FileSystemInfoViewModel>())
                    {
                        item.PropertyChanged += Item_PropertyChanged;               
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in args.NewItems.Cast<FileSystemInfoViewModel>())
                    {
                        item.PropertyChanged -= Item_PropertyChanged;
                    }
                    break;
            }
        }
        
        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "StatusMessage" && sender is FileSystemInfoViewModel viewModel)
                this.StatusMessage = viewModel.StatusMessage;
        }
        
        private void WatcherError(object sender, ErrorEventArgs e)
        {
            MessageBox.Show("Wystąpił błąd watchera: " + e.GetException().Message);
        }

        private void OnFileSystemChanged(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                IsNodeExpanded = true;
                loadTree(Path);
            });
        }

        public async Task Sort(SortModel sortModel)
        {
            Func<FileSystemInfoViewModel, IComparable> sortingOption = sortModel.OrderBy switch
            {
                OrderBy.Name => o => o.Model.Name,
                OrderBy.Created => o => o.Model.CreationTime,
                OrderBy.Modified => o => o.Model.LastWriteTime,
                OrderBy.Size => o => o.Size,
                OrderBy.Extension => o => o.Model.Extension,
                _ => o => o.Model.Name
            };

            var sorted = SortDirectoriesFromFiles();
            if (sortModel.OrderDir.Equals(OrderDir.Desc))
            {
                sorted = sorted.ThenByDescending(sortingOption);
            }
            else
            {
                sorted = sorted.ThenBy(sortingOption);
            }

            var sortedList = sorted.ToList();
            Task[] tasks = new Task[Items.Count];
            int i = 0;
            foreach (var item in sortedList)
            {
                tasks[i] = Task.Factory.StartNew(async () =>
                {
                    if (item is DirectoryInfoViewModel)
                    {
                        Globals.threadIds.Add(Thread.CurrentThread.ManagedThreadId);
                        Debug.WriteLine("[Thread "+Thread.CurrentThread.ManagedThreadId+"] Sorting: "+(item as DirectoryInfoViewModel)?.Model.FullName);
                        StatusMessage = "Sorting: " + (item as DirectoryInfoViewModel)?.Model.FullName;
                        await (item as DirectoryInfoViewModel)?.Sort(sortModel);
                    }
                }, Globals.token, sortModel.TaskCreationOptions, TaskScheduler.Default);
                i++;
                Items.Move(Items.IndexOf(item), sortedList.IndexOf(item));
            }

            Task.WaitAll(tasks);
            await Task.Delay(20);
        }

        private IOrderedEnumerable<FileSystemInfoViewModel> SortDirectoriesFromFiles()
        {
            return Items.OrderByDescending(o => (o.Model.Attributes & FileAttributes.Directory) != 0);
        }
    }
}