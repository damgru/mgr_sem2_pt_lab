﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace WpfApp1
{
    
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FileExplorer _fileExplorer;
        
        public MainWindow()
        {
            InitializeComponent();
            _fileExplorer = new FileExplorer();
            DataContext = _fileExplorer;
            _fileExplorer.PropertyChanged += _fileExplorer_PropertyChanged;
            _fileExplorer.OpenTextFileEvent += (o, txt) => textBlock.Text = txt;
        }

        private void _fileExplorer_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(FileExplorer.Lang))
                CultureResources.ChangeCulture(CultureInfo.CurrentUICulture);
        }

        private void OnDirDelete_Click(object sender, RoutedEventArgs e)
        {
            var node = (treeView.SelectedItem as DirectoryInfoViewModel).Model;
            try
            {
                Directory.Delete(node.FullName);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void OnCreateNewFile_Click(object sender, RoutedEventArgs e)
        {
            var directoryInfoViewModel = (treeView.SelectedItem as DirectoryInfoViewModel);
            var filePath = directoryInfoViewModel.Model.FullName;
            Dialog dialog = new Dialog(filePath);
            dialog.ShowDialog();
        }

        private void OnFileDelete_Click(object sender, RoutedEventArgs e)
        {
            var item = treeView.SelectedItem as FileInfoViewModel;
            var node = item.Model;
            try
            {
                File.Delete(node.FullName);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
        }

        private void OnFileOpen_Click(object sender, RoutedEventArgs e)
        {
            var filePath = (treeView.SelectedItem as FileInfoViewModel).Model.FullName;
            using (var textReader = File.OpenText(filePath))
            {
                textBlock.Text = textReader.ReadToEnd();
            }
        }

        private void MenuItem_ExitApp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void treeView_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            _fileExplorer.selectedItem(treeView.SelectedItem);
            if (treeView.SelectedItem is FileSystemInfoViewModel)
            {
                var treeViewSelectedItem = (treeView.SelectedItem as FileSystemInfoViewModel);
                string status = "";
                var item = treeViewSelectedItem.Model;
                if (item is FileSystemInfo)
                {
                    var attr = item.Attributes;
                    if ((attr & FileAttributes.System) != 0) status += "System,";
                    if ((attr & FileAttributes.Archive) != 0) status += "Archive,";
                    if ((attr & FileAttributes.Compressed) != 0) status += "Compressed,";
                    if ((attr & FileAttributes.Directory) != 0) status += "Directory,";
                    if ((attr & FileAttributes.Device) != 0) status += "Device,";
                    if ((attr & FileAttributes.Encrypted) != 0) status += "Encrypted,";
                    if ((attr & FileAttributes.Hidden) != 0) status += "Hidden,";
                    if ((attr & FileAttributes.IntegrityStream) != 0) status += "IntegrityStream,";
                    if ((attr & FileAttributes.NoScrubData) != 0) status += "NoScrubData,";
                    if ((attr & FileAttributes.Normal) != 0) status += "Normal,";
                    if ((attr & FileAttributes.NotContentIndexed) != 0) status += "NotContentIndexed";
                }
                statusText.Text = status;
            }
            else
            {
                statusText.Text = treeView.SelectedItem.GetType().ToString();
            }
        }

        private void TreeView_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (treeView.SelectedItem is FileInfoViewModel)
            {
                treeView.ContextMenu = treeView.Resources["FileContext"] as ContextMenu;
            }
            else
            {
                treeView.ContextMenu = treeView.Resources["FolderContext"] as ContextMenu;
            }
        }

        private void CancelSortButton(object sender, RoutedEventArgs e)
        {
            _fileExplorer.BtnVis = Visibility.Collapsed;
            Globals.tokenSource.Cancel();
            Globals.tokenSource.Dispose();

            _fileExplorer.StatusMessage = Strings.SortingStopped; 
        }
    }
    public class CultureResources
    {
        public Strings GetStringsInstance()
        {
            return new Strings();
        }
        
        private static ObjectDataProvider _provider;

        public static ObjectDataProvider ResourceProvider
        {
            get
            {
                if (_provider == null)
                    _provider =
                        (ObjectDataProvider)System.Windows.Application.Current.FindResource("Strings");
                return _provider;
            
            }
        }
        
        public static void ChangeCulture(CultureInfo culture)
        {
            ResourceProvider.Refresh();
        }
    }
}
