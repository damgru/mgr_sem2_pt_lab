﻿using System.Windows;
using WpfApp1.Models;

namespace WpfApp1
{
    public partial class SortDialog : Window
    {
        public SortDialog(SortModel model)
        {
            DataContext = model;
            InitializeComponent();
        }
    }
}