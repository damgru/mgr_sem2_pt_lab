﻿using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class SortModel : ViewModelBase
    {
        private OrderBy _orderBy = OrderBy.Name;

        public OrderBy OrderBy
        {
            get => _orderBy;
            set
            {
                _orderBy = value;
                OnPropertyChanged();
            }
        }

        private OrderDir _dir = OrderDir.Asc;

        public OrderDir OrderDir
        {
            get => _dir;
            set
            {
                _dir = value;
                OnPropertyChanged();
            }
        }

        private TaskCreationOptions _taskCreationOptions = TaskCreationOptions.PreferFairness;
        public TaskCreationOptions TaskCreationOptions
        {
            get => _taskCreationOptions;
            set
            {
                _taskCreationOptions = value;
                OnPropertyChanged();
            }
        }
    }

    public enum OrderBy
    {
        Name,
        Created,
        Modified,
        Size,
        Extension
    }

    public enum OrderDir
    {
        Asc,
        Desc
    }
}